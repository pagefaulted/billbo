#include <ArduinoHttpClient.h>
#include <WiFiNINA.h>
#include "data_retrieval.hpp"

const char *const SERVER_ADDRESS = "45.76.10.198";
const int SERVER_PORT = 7177;

WiFiClient wifi;
HttpClient client = HttpClient(wifi, "45.76.10.198", 7177);

String getUsQuotes(const char* const request) {
  client.get(request);

  if (client.responseStatusCode() == 200) {
    return client.responseBody();
  } else {
    // TODO: add error code, move strings to PROGMEM and use snprintf_P.
    Serial.println("Error retrieving US quotes: ");
    return client.responseBody();
  }
}
