/**
 * BillBo, rolling text led matrix for Arduino UNO.
 *
 * The display was built with a led strip based on the WS2812B controller, where each led is addressable in isolation.
 *
 * For my specific needs, the led strip density is 60 leds/meter, 7 rows of 41 leds each, but the code can
 * be adapted to support any density/matrix size via the defined constants (see LED_COUNT, LED_COLUMNS and FONT_HEIGHT).
 *
 * The fonts are represented as arrays of uint8_t of size FONT_WIDTH (5 in my case), where each element's bits represents a led in a column (in my case bits 7 to 1, for FONT_HEIGHT of 7).
 *
 * The implementation details are inevitably affected by the hardware, the Adafruit API as well as how the rows were soldered together.
 *
 * The led strip was cut to size and soldered together at the extremities as below:
 *
 *             0  1  2  3  4  5  6  7
 *              o  o  o  o  o  o  o  o
 *                                   ] soldering
 *             o  o  o  o  o  o  o  o
 *            15 14 13 12 11  10 9  8
 *  soldering [
 *            16
 *             o  o  o  o  o  o  o  o
 *                                   ] soldering
 *             o  o  o  o  o  o  o  o
 *
 * Whilst this approach made the soldering quite easy, it created the need for an awkward mapping between the font's matrix representation and the fact
 * that leds are addressed by index like an array.
 *
 */

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include "constants.hpp"
#include "billbo.hpp"
#include "wifi.hpp"
#include "data_retrieval.hpp"

Adafruit_NeoPixel board(LED_COUNT, LED_PIN, NEO_GBR + NEO_KHZ800);
uint32_t color = board.Color(0, 0, 255);

void setup() {
  board.begin();
  board.show();
  board.setBrightness(LED_BRIGHTNESS);
  Serial.begin(9600);
  
  if (initWifi()) {
    Serial.println("Failed to initialize WiFi.");
    exit(1);
  }
}

void loop() {
  String response = getUsQuotes(QUOTES_REQUEST);
  scroll_string(response.c_str());

  delay(LOOP_DELAY);
}

/**
 * Maps the position in the matrix to the actual led id. [0][0] is the top-right-most led.
 *
 * The matrix is actually an array, each led is accessed via a single index.
 *
 * The need for this elaborate mapping comes from the fact that the letters are
 * are represented as leds in a matrix, but the hardware is in fact a bunch of leds
 * in series, like an array, addressed by a single index.
 *
 * Due to how the led rows are soldered together, the indexing is increasing left to right
 * for even rows and increasing right to left for odd rows, as follows:

            0  1  2  3  4  5  6  7
            o  o  o  o  o  o  o  o
                                  ] soldering
            o  o  o  o  o  o  o  o
           15 14 13 12 11  10 9  8
 soldering [
           16
            o  o  o  o  o  o  o  o
                                  ] soldering
            o  o  o  o  o  o  o  o
 *
 */
uint16_t position_2_led(const uint8_t column_idx, const uint8_t row_idx) {
  const uint16_t starting_pixel = column_incipit[row_idx];
  const uint16_t led_idx = row_idx % 2 == 0 ? (starting_pixel - column_idx) : (starting_pixel + column_idx);
  return led_idx;
}

/**
 * Scroll the provided string across the led matrix.
 */
void scroll_string(const char *c) {
  const uint16_t word_length = strlen(c);
  const uint16_t columns_count = (word_length * FONT_WIDTH) + (word_length * INTERCHAR_SPACE);
  uint8_t word_columns[columns_count] = { 0 };
  for(uint16_t letter_idx = 0; letter_idx < word_length; ++letter_idx) {
    const uint8_t *letter = fonts[*(c + letter_idx) - ASCII_OFFSET];
    for(uint16_t l_column = 0; l_column < FONT_WIDTH; ++l_column) {
      // Read from PROGMEM and discard irrelevant bit
      const uint16_t column = pgm_read_byte_near(letter + l_column) >> 1;
      word_columns[(letter_idx * FONT_WIDTH) + l_column + (letter_idx * INTERCHAR_SPACE)] = column;
    }
  }

  // Amount of iterations to scroll the whole string across the matrix.
  const uint16_t required_iterations = LED_COLUMNS + columns_count;

  for(uint16_t i = 0; i < required_iterations; ++i) {
    board.clear();
    for(uint16_t starting_column = i, word_column = 0; starting_column >= 0 && word_column < columns_count; --starting_column, ++word_column) {
      const uint8_t column = word_columns[word_column];
      for(int8_t pixel_idx = 6, h = 0; pixel_idx >= 0; --pixel_idx, ++h) {
        const uint8_t pixel_value = (column >> pixel_idx) & 0x01;
        if (pixel_value && starting_column >= 0 && starting_column < LED_COLUMNS) {
          const uint16_t led_idx = position_2_led(starting_column, h);
          board.setPixelColor(led_idx, color);
        }
      }
    }
    board.show();
    delay(SCROLL_DELAY);
  }
}

/**
 * Print the provided char on the led matrix, starting from the left, at the provided char position.
 */
void print_char(const char c, const uint8_t char_position) {
  const uint16_t fonts_count = sizeof(fonts);
  const uint8_t char_idx = c - ASCII_OFFSET;
  if (char_idx < fonts_count) {
    const uint8_t *font = fonts[char_idx];
    for(uint8_t i = 0; i < FONT_WIDTH; ++i) {
      const uint8_t pixels = pgm_read_byte_near(font + i) >> 1;
      for(int8_t j = FONT_HEIGHT - 1, h = 0; j >= 0; --j, ++h) {
        const uint8_t pixel = (pixels >> j) & 0x01;
        if (pixel) {
          // Account for the fact that [0][0] is the top right led.
          const uint8_t column_idx = LED_COLUMNS - 1 - i - (char_position * FONT_WIDTH);
          board.setPixelColor(position_2_led(column_idx, h), color);
        }
      }
    }
  } else {
    Serial.print("Unknown char: ");
    Serial.println(c);
  }
}

/**
 * Print the provided string on the led matrix.
 */
void print_str(const char *const str) {
  board.clear();
  
  const uint8_t len = strlen(str);
  for(uint8_t i = 0; i < len; ++i) {
    print_char(*(str + i), i);
  }
  
  board.show();
}

void all_on() {
  board.clear();
  for(uint8_t i = 0; i < LED_COUNT; ++i) {
    board.setPixelColor(i, color);
  }
  board.show();
}

void all_off() {
  board.clear();
  board.show();
}
