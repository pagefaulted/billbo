#include <Arduino.h>
#include <ArduinoHttpClient.h>
#include <WiFiNINA.h>
#include "wifi.hpp"
#include "secrets.hpp"

const uint8_t RETRY_COUNT = 16;

void printWifiStatus() {
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

int initWifi() {
  int status = WL_IDLE_STATUS;

  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");

    return 1;
  }
  Serial.println("WiFi module ready.");

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware.");
  }

  uint8_t retries = 0;
  while (status != WL_CONNECTED && ++retries <= RETRY_COUNT) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);

    status = WiFi.begin(ssid, pwd);

    delay(10000);
  }

  if (status != WL_CONNECTED) {
      Serial.println("Could not connect to WiFi!");
      return 1;
  }

  Serial.println("Connected to WiFi!");
  printWifiStatus();

  return 0;
}
