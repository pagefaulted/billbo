#ifndef BILLBO_HPP
#define BILLBO_HPP

uint16_t position_2_led(uint8_t column_idx, uint8_t row_idx);
void scroll_string(const char *c);
void print_char(char c, uint8_t char_position);
void print_str(char *str);
void all_on();
void all_off();
void printWifiStatus();

#endif