#ifndef BILLBO_DATA_RETRIEVAL
#define BILLBO_DATA_RETRIEVAL

/**
 * Retrieves the quotes for the tickers in the request char*.
 */
String getUsQuotes(const char* const request);

#endif