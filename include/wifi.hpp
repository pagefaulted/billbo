#ifndef BILLBO_WIFI
#define BILLBO_WIFI

/**
 * Initialize wifi module.
 *
 * Returns 0 on success, 1 on error.
 */
int initWifi();

#endif