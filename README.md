# BillBo, rolling text led matrix.

The display was built with a led strip based on the `WS2812B` controller, where each led is addressable in isolation.

For my specific needs, the led strip density is 60 leds/meter, 7 rows of 41 leds each, but the code can
be adapted to support any density/matrix size via the defined constants (see `LED_COUNT`, `LED_COLUMNS` and `FONT_HEIGHT`).

The fonts are represented as arrays of `uint8_t` of size `FONT_WIDTH` (5 in my case), where each element's bits represents a led in a column (in my case bits 7 to 1, for `FONT_HEIGHT` of 7).

The implementation details are inevitably affected by the hardware, the Adafruit API as well as how the rows were soldered together.

The led strip was cut to size and soldered together at the extremities as below:
```
            0  1  2  3  4  5  6  7
             o  o  o  o  o  o  o  o
                                  ] soldering
            o  o  o  o  o  o  o  o
           15 14 13 12 11  10 9  8
 soldering [
           16
            o  o  o  o  o  o  o  o
                                  ] soldering
            o  o  o  o  o  o  o  o
```
Whilst this approach made the soldering quite easy, it created the need for an awkward mapping between the font's matrix representation and the fact
that leds are addressed by index like an array.


